/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.nakamol.exhaustivesearch;

import java.util.ArrayList;

/**
 *
 * @author OS
 */
public class ExhaustiveSearch {

    private int[] input;
    static int arrIndex = 0;
    static ArrayList<ArrayList<Integer>> Arr = new ArrayList<ArrayList<Integer>>();
    static ArrayList<ArrayList<Integer>> Arr1 = new ArrayList<ArrayList<Integer>>();

    public ExhaustiveSearch(int[] input) {
        this.input = input;
    }

    public void process() {
        ArrayList<Integer> trash = new ArrayList<>();
        ArrayList<Integer> trash1 = new ArrayList<>();

        for (int o = 0; o < input.length; o++) {
            trash.add(input[o]);
        }
        for (int i = 0; i < input.length; i++) {
            trash1.add(input[i]);
        }

        int arr[] = input;
        int n = arr.length;

        for (int i = input.length; i >= 0; i--) {
            preArr(arr, n, i);
        }

        arrIndex = 1;
        Arr1.add(0, trash1);
        for (int j = 1; j < Arr.size(); j++) {
            trash = new ArrayList<>();
            for (int i = 0; i < input.length; i++) {
                trash.add(input[i]);
            }
            for (int k = 0; k < Arr.get(j).size(); k++) {
                for (int u = 0; u < trash.size(); u++) {
                    if (trash.get(u) == Arr.get(j).get(k)) {
                        trash.remove(u);
                        u--;
                    }
                }
            }
            Arr1.add(arrIndex, trash);
            arrIndex++;
        }
    }

    private void preArr(int[] arr, int n, int m) {
        int data[] = new int[m];
        combination(arr, data, 0, n - 1, 0, m);
    }

    static void combination(int arr[], int data[], int start, int end, int index, int m) {
        ArrayList<Integer> trash = new ArrayList<>();
        if (index == m) {
            for (int j = 0; j < m; j++) {
                trash.add(data[j]);
            }
            Arr.add(arrIndex, trash);
            return;
        }

        for (int i = start; i <= end && end - i + 1 >= m - index; i++) {
            data[index] = arr[i];
            combinationUntil(arr, data, i + 1, end, index + 1, m);
        }

    }

    private static void combinationUntil(int arr[], int data[], int start, int end, int index, int m) {
        ArrayList<Integer> trash = new ArrayList<>();
        if (index == m) {
            for (int j = 0; j < m; j++) {
                trash.add(data[j]);
            }
            Arr.add(arrIndex, trash);
            return;
        }

        for (int i = start; i <= end && end - i + 1 >= m - index; i++) {
            data[index] = arr[i];
            combinationUntil(arr, data, i + 1, end, index + 1, m);
        }
    }

    public void getDisjoinArray() {
        for (int i = 0; i < Arr.size(); i++) {
            for (int j = 0; j < Arr.get(i).size(); j++) {
                System.out.print(Arr.get(i).get(j) + " ");
            }
            System.out.println("");
        }
    }

    public void getDisjoinArray1() {
        for (int i = 0; i < Arr1.size(); i++) {
            for (int j = 0; j < Arr1.get(i).size(); j++) {
                System.out.print(Arr1.get(i).get(j) + " ");
            }
            System.out.println("");
        }
    }

    public void sum() {
        for (int i = 0; i < Arr.size(); i++) {
            if (Arr.contains(Arr1.get(i))) {
                Arr1.remove(i);
                Arr.remove(i);
            }
        }

        for (int i = 0; i < Arr.size(); i++) {
            int sumN = 0;
            int sumM = 0;
            for (int j = 0; j < Arr.get(i).size(); j++) {
                sumN += Arr.get(i).get(j);
            }

            for (int j = 0; j < Arr1.get(i).size(); j++) {
                sumM += Arr1.get(i).get(j);
            }

            if (sumN == sumM) {
                System.out.println(Arr.get(i) + " " + Arr1.get(i));
            }
        }
    }

}
