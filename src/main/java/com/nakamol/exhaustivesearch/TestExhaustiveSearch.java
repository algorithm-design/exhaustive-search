/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nakamol.exhaustivesearch;

/**
 *
 * @author OS
 */
public class TestExhaustiveSearch {

    public static void main(String[] args) {
        int[] x = {1, 3, 4, 5, 7, 8, 10};
        showInput(x);
        ExhaustiveSearch es = new ExhaustiveSearch(x);
        es.process();
//        es.getDisjoinArray();
//        System.out.println("");
        es.getDisjoinArray1();
        es.sum();
    }

    private static void showInput(int[] x) {
        System.out.println("Input is: ");
        for (int i = 0; i < x.length; i++) {
            System.out.println(x[i] + " ");
        }

        System.out.println(" ");
    }
}
